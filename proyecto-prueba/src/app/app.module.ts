import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PruebaComponComponent } from './prueba-compon/prueba-compon.component';
import { XItemComponent } from './x-item/x-item.component';
import { ListadoItemComponent } from './listado-item/listado-item.component';

@NgModule({
  declarations: [
    AppComponent,
    PruebaComponComponent,
    XItemComponent,
    ListadoItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
