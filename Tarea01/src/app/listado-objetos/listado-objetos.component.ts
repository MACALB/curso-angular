import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-listado-objetos',
  templateUrl: './listado-objetos.component.html',
  styleUrls: ['./listado-objetos.component.css']
})
export class ListadoObjetosComponent implements OnInit {

  capitales: string [];
  listaItem: string[];
  @HostBinding ('attr.class') cssClass = 'container';
  @HostBinding ('style.color') color = 'red';

  constructor() { }

  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    // this.capitales = [ 'Saint John', 'Buenos Aires', 'Nasáu​Bridgetown', 'Belmopán', 'Sucre y La Paz', 'Brasilia', 'Ottawa', 'Santiago', 'Bogotá', 'San José', 'La Habana', 'Roseau', 'Quito', 'San Salvador', 'Washington D. C.', 'Saint George', 'Ciudad de Guatemala', 'Georgetown', 'Puerto Príncipe', 'Tegucigalpa', 'Kingston', 'Ciudad de México', 'Managua', 'Ciudad de Panamá', 'Asunción', 'Lima', 'Santo Domingo', 'Basseterre', 'Kingstown', 'Castries', 'Paramaribo', 'Puerto España', 'Montevideo', 'Caracas' ];
    // tslint:disable-next-line: max-line-length
    this.capitales = [ 'Saint John', 'Buenos Aires', 'Nasáu​Bridgetown', 'Belmopán', 'Sucre y La Paz' ];
    // tslint:disable-next-line: max-line-length
    // this.listaItem = ['Antigua y Barbuda', 'Argentina', 'Bahamas', 'Barbados', 'Belice', 'Bolivia', 'Brasil', 'Canada',   'Chile', 'Colombia', 'Costa Rica', 'Cuba', 'Dominica', 'Ecuador', 'El Salvador', 'Estados UNidos', 'Granada', 'Guatemala', 'Guyana', 'Haití', 'Honduras', 'Jamaica', 'México', 'Nicaragua', 'Panamá', 'Paraguay', 'Perú', 'República Dominicana', 'San Cristobal y Nieves', 'San Vicente y las Granadinas', 'Santa Lucia', 'Surinam', 'Trinidad y Tobago', 'Uruguay', 'Venezuela'];
    // tslint:disable-next-line: max-line-length
    this.listaItem = ['Antigua y Barbuda', 'Argentina', 'Bahamas', 'Barbados'];

  }

  addNewItem(nombrePais: string): boolean {

    this.listaItem.push(nombrePais);
    // console.log(new DestinoViaje(nombre, url));
    console.log(this.listaItem);
    // console.log(newItemDescribe);
    return false;
  }

}
