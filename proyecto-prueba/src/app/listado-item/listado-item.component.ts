import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado-item',
  templateUrl: './listado-item.component.html',
  styleUrls: ['./listado-item.component.css']
})
export class ListadoItemComponent implements OnInit {

  listaItem: string[];

  constructor() { }

  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    this.listaItem = ['Antigua y Barbuda', 'Argentina', 'Bahamas', 'Barbados', 'Belice', 'Bolivia', 'Brasil', 'Canada',   'Chile', 'Colombia', 'Costa Rica', 'Cuba', 'Dominica', 'Ecuador', 'El Salvador', 'Estados UNidos', 'Granada', 'Guatemala', 'Guyana', 'Haití', 'Honduras', 'Jamaica', 'México', 'Nicaragua', 'Panamá', 'Paraguay', 'Perú', 'República Dominicana', 'San Cristobal y Nieves', 'San Vicente y las Granadinas', 'Santa Lucia', 'Surinam', 'Trinidad y Tobago', 'Uruguay', 'Venezuela'];
  }

}
