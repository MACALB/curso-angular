import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaComponComponent } from './prueba-compon.component';

describe('PruebaComponComponent', () => {
  let component: PruebaComponComponent;
  let fixture: ComponentFixture<PruebaComponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaComponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaComponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
