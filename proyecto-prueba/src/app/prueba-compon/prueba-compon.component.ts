import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prueba-compon',
  templateUrl: './prueba-compon.component.html',
  styleUrls: ['./prueba-compon.component.css']
})
export class PruebaComponComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  agregar(titulo: HTMLInputElement)	{
    console.log(titulo.value);
  }

}
