import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XItemComponent } from './x-item.component';

describe('XItemComponent', () => {
  let component: XItemComponent;
  let fixture: ComponentFixture<XItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
