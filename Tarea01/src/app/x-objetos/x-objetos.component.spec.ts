import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XObjetosComponent } from './x-objetos.component';

describe('XObjetosComponent', () => {
  let component: XObjetosComponent;
  let fixture: ComponentFixture<XObjetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XObjetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XObjetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
