import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-x-item',
  templateUrl: './x-item.component.html',
  styleUrls: ['./x-item.component.css']
})
export class XItemComponent implements OnInit {

  @Input() x: string;

  constructor() {
    // this.x = 'Nombre pais';
   }

  ngOnInit() {
  }

}
